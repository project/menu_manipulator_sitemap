CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Display a very simple sitemap filtered by the Menu Manipulator module.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/menu_manipulator_sitemap

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/menu_manipulator_sitemap


REQUIREMENTS
------------

This module requires following modules outside of Drupal core:

 * Menu Manipulator - https://www.drupal.org/project/menu_manipulator



INSTALLATION
------------

 * Install the Menu Manipulator Sitemap module as you would normally install a
   contributed Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > > Structure > Block layout and place
       Menu Manipulator - Sitemap block.
    3. Click Save blocks at the bottom of the page.


MAINTAINERS
-----------

 * Matthieu Scarset (matthieuscarset) - https://www.drupal.org/u/matthieuscarset
