<?php

namespace Drupal\menu_manipulator_sitemap\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\system\Entity\Menu;

/**
 * Provides a block to display a Sitemap.
 *
 * @Block(
 *   id = "menu_manipulator_sitemap_block",
 *   admin_label = @Translation("Menu Manipulator - Sitemap"),
 * )
 */
class MenuManipulatorSitemapBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'menus'  => [],
      'orders' => [],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    // Load defaults.
    $options   = [];
    $selection = $this->configuration['menus'];

    // Load menus.
    $menus = \Drupal::entityTypeManager()->getStorage('menu')->loadMultiple();
    foreach ($menus as $menu) {
      $options[$menu->id()] = $menu->label();
    }

    // Display menus to be selected.
    $form['menus'] = [
      '#type'          => 'checkboxes',
      '#title'         => $this->t('Menus'),
      '#options'       => $options,
      '#default_value' => $selection,
    ];

    // Display options to reorder menus,
    $form['orders'] = [
      '#type'        => 'details',
      '#title'       => $this->t('Order'),
      '#collapsible' => TRUE,
      '#open'        => TRUE,
      '#access'      => !empty($selection),
      '#description' => $this->t('Reorder menus included in Sitemap'),
    ];
    foreach ($selection as $menu_name => $activated) {
      if (!$activated) {
        continue;
      }

      $form['orders'][$menu_name] = [
        '#title'         => $options[$menu_name],
        '#type'          => 'number',
        '#min'           => 0,
        '#max'           => count($menus),
        '#default_value' => $this->configuration['orders'][$menu_name] ?? 0,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $values                        = $form_state->getValues();
    $this->configuration['menus']  = $values['menus'] ?? [];
    $this->configuration['orders'] = $values['orders'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    $build['sitemap'] = [
      '#type'       => 'container',
      '#attributes' => ['class' => 'sitemap'],
    ];

    // Load content from menus.
    $menus     = [];
    $orders    = $this->configuration['orders'] ?? [];
    $selection = $this->configuration['menus'] ?? [];

    foreach ($selection as $menu_name => $activated) {
      if (!$activated) {
        continue;
      }
      // Inject menus with desired order.
      if (isset($orders[$menu_name])) {
        $menus[$orders[$menu_name]] = $menu_name;
      }
    }
    // Sort menus.
    ksort($menus);

    foreach ($menus as $menu_name) {
      $menu = Menu::load($menu_name);

      $build['sitemap'][$menu_name] = [
        '#theme'      => 'item_list',
        '#list_type'  => 'ul',
        '#items'      => [],
        '#attributes' => ['class' => 'sitemap-menu'],
        '#title'      => $menu->label(),
      ];

      $menu = menu_manipulator_get_multilingual_menu($menu_name);
      foreach (($menu['#items'] ?? []) as $key => $item) {
        $build['sitemap'][$menu_name]['#items'][$key] = $this->renderMenuLinkItem($item, 'sitemap-menu-item');

        if (!empty($item['below'])) {
          $build['sitemap'][$menu_name]['#items'][$key]['submenu'] = [
            '#theme'      => 'item_list',
            '#list_type'  => 'ul',
            '#items'      => [],
            '#attributes' => ['class' => 'sitemap-submenu'],
          ];

          foreach ($item['below'] as $item) {
            $build['sitemap'][$menu_name]['#items'][$key]['submenu']['#items'][] = $this->renderMenuLinkItem($item, 'sitemap-submenu-item');
          }
        }
      }
    }

    $build['#cache'] = [
      'contexts' => parent::getCacheContexts(),
      'tags'     => parent::getCacheTags(),
    ];

    return $build;
  }

  protected function renderMenuLinkItem($item, $class = '') {
    $output = [];

    $output['#markup'] = Link::fromTextAndUrl(
      $item['title'],
      $item['url']->setOption('attributes', ['class' => $class])
    )->toString();

    return $output;
  }
}
